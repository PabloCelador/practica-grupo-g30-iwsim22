package src;

public class JuegoEntrenamiento extends Conecta4 {
    public JuegoEntrenamiento(InOut io) {
        super(io);
        GestorDeJugadas gestorDeJugadas = new GestorDeJugadas(io);
        Jugador jugador1 = new JugadorMaquina(Ficha.ROJO, io);
        Jugador jugador2 = new JugadorHumano(Ficha.AMARILLO, gestorDeJugadas, io);
        turno = new Turno(new Jugador[]{jugador1, jugador2});
    }
}